import copy
from typing import Any, Dict, Optional

from credit import Credit


class CreditDb():

  def __init__(self):
    self.db = {}

  def clear(self):
    self.db = {}

  def save(self, credit: Credit) -> bool:
    if not credit or not credit.credit_id:
      return False
    self.db[credit.credit_id] = credit
    return True

  def get(self, credit_id: str) -> Optional[Credit]:
    return self.db.get(credit_id)

  def get_all(self) -> Dict[str, Credit]:
    return copy.deepcopy(self.db)

  def delete(self, credit_id: str):
    if credit_id in self.db:
      del self.db[credit_id]
      return True
    return False
