import datetime
import uuid
from typing import Optional

from util import datetime_to_string, get_current_datetime_utc


class Credit():

  def __init__(self,
               user_id: int,
               amount: float,
               created_at: Optional[datetime.datetime] = None,
               credit_id: Optional[str] = None,
               expires_at: Optional[datetime.datetime] = None):
    now = get_current_datetime_utc()
    self.user_id = user_id
    self.amount = amount
    self.created_at = created_at or get_current_datetime_utc()
    self.credit_id = credit_id or str(uuid.uuid4())
    self.expires_at = expires_at

  def __str__(self):
    return str({
      'credit_id': self.credit_id,
      'user_id': self.user_id,
      'amount': round(self.amount, 2),
      'expires_at': datetime_to_string(self.expires_at),
    })
