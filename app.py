import datetime
import json
import uuid
from typing import Any, Dict, Optional

from flask import Flask, jsonify, make_response, request

import credit_db
from credit import Credit
from util import get_current_datetime_utc

app = Flask(__name__)

credit_db_inst = credit_db.CreditDb()


@app.route('/users/<user_id>/credits')
def get_user_credits(user_id):
  '''Returns the user's credit balance.'''
  balance = 0
  all_credits = credit_db_inst.get_all()
  print('\n'.join([str(cred) for _, cred in all_credits.items()]))
  now = get_current_datetime_utc()
  for _, credit in all_credits.items():
    if credit.user_id != int(user_id):
      continue
    if credit.expires_at and credit.expires_at <= now:
      continue
    balance += credit.amount
  return make_response(jsonify({'balance': balance}), 200)


@app.route('/users/<user_id>/credits', methods=['POST'])
def add_credit(user_id):
  '''Add a credit for a user. Raises an error if credit balance will drop below zero.
  
  Body params:
    -amount [required]
    -expires_at [defaults to None]
  '''
  body = json.loads(request.data)
  amount = float(body['amount'])
  if amount < 0:
    balance = json.loads(get_user_credits(user_id).data)['balance']
    if balance + amount < 0:
      raise Exception('Not permitted - credits will drop below zero')
  new_credit = Credit(
    int(user_id),
    amount,
    expires_at=body.get('expires_at'))
  credit_db_inst.save(new_credit)
  return 'Success'


@app.route('/seed_db')
def seed_db():
  '''Seed the credit database.'''
  now = get_current_datetime_utc()
  earned_credit = {
    'credit_id': str(uuid.uuid4()),
    'user_id': 1,
    'amount': 20,
    'expires_at': now + datetime.timedelta(days=362),
  }
  consumed_credit = {
    'credit_id': str(uuid.uuid4()),
    'user_id': 1,
    'amount': -15,
    'expires_at': None,
    'created_at': now + datetime.timedelta(days=30)
  }
  for credit_dict in [earned_credit, consumed_credit]:
    credit_db_inst.save(Credit(**credit_dict))
  return 'Success'


@app.route('/clear_db', methods=['DELETE'])
def clear_db():
  '''Clear the credits database.'''
  credit_db_inst.clear()
  return 'Success'
