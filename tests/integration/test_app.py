import json
import unittest
from unittest import mock

import app

class TestApp(unittest.TestCase):

  def setUp(self):
    self.app = app.app.test_client()
    self.app.get('/seed_db')

  def tearDown(self):
    self.app.delete('/clear_db')

  def test_get_user_credits(self):
    result = self.app.get('/users/1/credits')
    self.assertEqual(200, result.status_code)
    self.assertDictEqual({'balance': 5}, json.loads(result.data))

  @mock.patch('app.credit_db_inst')
  def test_custom_db(self, mock_credit_db):
    mock_credit_db.return_value.get_all.return_value = {}
    result = self.app.get('users/1/credits')
    self.assertEqual(200, result.status_code)
    self.assertDictEqual({'balance': 0}, json.loads(result.data))
